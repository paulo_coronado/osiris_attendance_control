import pandas as pd
from os import listdir

class Generator:

    def __init__(self):
        self.file_names=[]
        self.dicts=[]
        pass
        
    def get_statistics(self, dir_path):
        
        # Get the file name of the spreadsheet in dir_path
        
        files= listdir(dir_path)
        
        for file in files:
            if file.endswith(".xlsx"):
                file_name = file
                self.file_names.append(file_name)
        
           
    def read_spreadsheet(self, file_name):
        
        #Read all sheets in the spreadsheet
        #read_excel() returns a dictionary of dataframes. The keys are the sheet names.
        dict=pd.read_excel(file_name, sheet_name=None)
        
        #Add the dictionary to the list of dictionaries
        self.dicts.append(dict)
        
        #Return the dictionary
        return dict
    
    def parse_student_data(self, df, group):
        
        #Create an empty dataframe to hold the parsed data
        parsed_df=pd.DataFrame(columns=['identificación', 'nombre','curso'])
        #Iterate through the dataframe                    
        for index in range(len(df.index)):
            #Get the row as a pandas series
            row=df.iloc[index]
            
            #First column is an index, so we skip it
            
            #Try to convert second column to a number
            try:
                #Second column is the card id converted to an integer
                card_id=int(row[1])    
                
                #Third column is the name
                name=row[2]
                
                #Add the parsed data to the dataframe if the card_id is greater than 0
                if card_id>0:                   
                    parsed_df.loc[len(parsed_df)]=[card_id, name, group]
            except ValueError as e:
                #Print e
                print(e)
                continue
        return parsed_df
    
    def create_master_list(self):        
        print("Starting...")
        student_groups=[]
        #Iterate through the list of dictionaries    
        for dict in self.dicts:                      
            #Iterate through the dictionary of dataframes
            for key, val in dict.items():
                #key is the sheet name
                #val is the dataframe
                #Add the dataframe to the list of dataframes
                student_groups.append(self.parse_student_data(val, key))
        #Concatenate the dataframes into one dataframe
        master_list=pd.concat(student_groups)
        
        #Change index to year from 1900 to 2020
        master_list.index=range(len(master_list.index))        
        #Save the dataframe to a xlsx file
        master_list.to_excel("master_list.xlsx")

#Generate the master list
directory= 'listas_estudiantes'
myProcessor=Generator()
myProcessor.get_statistics(directory)
for name in myProcessor.file_names:
    myProcessor.read_spreadsheet(directory+'/'+name)

myProcessor.create_master_list()



        
    