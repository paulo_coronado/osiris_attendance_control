import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join



class Processor:
    
    
    def __init__(self):
        self.file_names=[]
        #Open the master list spreadsheet and assign it to a variable
        if isfile("master_list.xlsx"):
            self.master_list=pd.read_excel("master_list.xlsx")
            print(self.master_list.head())
            print(self.master_list.identificación.dtype)
        else:
            #Raise an error if the master list spreadsheet does not exist
            raise Exception("Master list spreadsheet does not exist")

                
    def get_statistics(self, dir_path):
        
        # Get the file name of the spreadsheet in the folder registro_ingresos
        
        files= listdir(dir_path)
        
        for file in files:
            if file.endswith(".xlsx"):
                file_name = file
                self.file_names.append(file_name)
        print (self.file_names)
        
    
    def parse_spreadsheet(self, file_name):
        
        #Extract the date from the file name
        date=file_name.split("/")[1].split(".")[0]
        
        #Create a dataframe 
        df_attendance=pd.DataFrame(index=np.arange(len(self.master_list)), columns=[str(date)], data=0)
        # Read the spreadsheet
        df=pd.read_excel(file_name, header=None)
        
        
        #If it exists, parse the data
        #Create an empty dataframe with one column called 'data'
        parsed_df=pd.DataFrame(columns=['data'])
        
        #Read each column in the spreadsheet
        for column in df.columns:
            df.rename(columns = {column:'data'}, inplace = True)
            #Concat the dataframe with the parsed dataframe 
            parsed_df=pd.concat([parsed_df, df[['data']]],ignore_index=True)    
            #Remove the column from the dataframe
            df.drop(columns=['data'], inplace=True)
        
        #Drop rows with no data
        parsed_df.dropna(inplace=True)    
        
        
        
        #Check if the students in parsed_df are in the master list
        for index in range(len(parsed_df.index)):       
            row=parsed_df.iloc[index]
            try:
                student=int(row['data'])                
                #Return the row id if the student is in the master list
                if student in self.master_list.identificación.values:
                    row_id=self.master_list.loc[self.master_list['identificación']==student].index.values[0]
                    #print("I found the student "+str(student)+" in row " + str(row_id))  
                    #Add 1 to the attendance column in the dataframe
                    df_attendance.iloc[row_id][str(date)]=1                                   
                else:
                    #print("The student "+str(student)+" is not in the master list")
                    continue
            except ValueError as e:
                print(e)
                continue
        #Add the dataframe to the master list
        self.master_list=pd.concat([self.master_list, df_attendance], axis=1)
        return True
        
    def create_master_list(self):
        #Save the master list spreadsheet
        self.master_list.to_excel("consolidated_master_list.xlsx")
        return True
       
                        
            
        
directory= 'registros_ingreso'
myProcessor=Processor()
myProcessor.get_statistics(directory)
for name in myProcessor.file_names:
    myProcessor.parse_spreadsheet(directory+'/'+name)
myProcessor.create_master_list()


        
    